import re
import itertools
from time import sleep

PuzzleInput = '1321131112'

def GetNewString(s):
	new_s = ''
	for k, v in itertools.groupby(s):
		l = list(v)
		new_s += str(len(l))
		new_s += l[0]

	return new_s

s = PuzzleInput
for i in range(50):
	print i
	s = GetNewString(s)
	if i % 39 == 0:
		print 'Part 1: ' + str(len(s))
print 'Part 2: ' + str(len(s))
