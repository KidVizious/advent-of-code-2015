import re

PuzzleInput = 'hepxxyzz'

'''
Rules:
- Include one straight of three letters (cannot skip letters)
- May not contain i, o, or l
- Must contain at least two different, non-overlapping pairs of letters (aa,bb,zz)
'''

def FindStraight(s):
	v = bytearray(s)
	for i in range(len(v) - 2):
		char = v[i]
		if v[i + 1] == char + 1 and v[i + 2] == char + 2:
			return True
	return False

def Increment(val):
	v = int(val,base=36)
	v += 1
	v = ConvertFromBase36(v)
	return v

def ConvertFromBase36(val):
	result = []
	while val:
		val, mod = divmod(val,36)
		result.append('0123456789abcdefghijklmnopqrstuvwxyz'[mod])
	result = ''.join(reversed(result))
	result = result.replace('0','a')
	return result

Done = False
NewPass = PuzzleInput
while not Done:
	#Increment the password
	NewPass = Increment(NewPass)
	#Password can not have i, l or o
	if 'i' in NewPass or 'l' in NewPass or 'o' in NewPass:
		continue

	#Find two pairs of repeating characters
	pattern = re.compile(r'(.)\1{1}')
	match = re.findall(pattern, NewPass)
	if len(match) < 2:
		continue

	#Find a straight of 3
	if not FindStraight(NewPass):
		continue
	else:
		Done = True
print NewPass
