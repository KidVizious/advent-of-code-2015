import re

InputFile = open('input.txt','r')

#List of attendee names
Names = []
Happiness = {}
Lines = []
#Pattern of input file
pattern = re.compile('(.*) would (gain|lose) (-?\d+) happiness units by sitting next to (.*)\.')
for Line in InputFile:
	Lines.append(Line)

#Close input file
InputFile.close()

for Line in Lines:
	#Pull desired parts out of string
	Name1, Measure, Value, Name2 = re.match(pattern,Line).groups()
	if Name1 not in Names:
		Names.append(Name1)
	if Name2 not in Names:
		Names.append(Name2)

for name1 in Names:
	for name2 in Names:
		if name1 != name2:
			Happiness[frozenset((name1, name2))] = 0

for Line in Lines:
	#Pull desired parts out of string
	Name1, Measure, Value, Name2 = re.match(pattern,Line).groups()
	Value = int(Value)
	if Measure == 'lose':
		Value = -Value
	Happiness[frozenset((Name1,Name2))] += Value

Names = set(Names)

def FindBest(Names, Start, End):
	if len(Names) == 0:
		return Happiness[frozenset((Start,End))]
	Best = -9999999999
	for NextName in Names:
		Best = max(Best, Happiness[frozenset((Start,NextName))] + FindBest(Names.difference({NextName}), NextName, End))
	return Best

StartingName = Names.pop()
Happiest = FindBest(Names, StartingName, StartingName)
print Happiest
