from collections import Counter

#Create a dictionary to hold visited locations
Visited = {}

#Class to store current location as Santa moves around
class Location:
	def __init__(self):
		self.x = 0
		self.y = 0
	def MoveNorth(self):
		self.y = self.y + 1
	def MoveSouth(self):
		self.y = self.y - 1
	def MoveEast(self):
		self.x = self.x + 1
	def MoveWest(self):
		self.x = self.x - 1
	def GetString(self):
		return str(self.x) + ',' + str(self.y)

def SaveVisit(loc):
	#Get string representation of our current location
	CurrentHouse = loc.GetString()
	if CurrentHouse in Visited:
		Visited[CurrentHouse] = Visited[CurrentHouse] + 1
	else:
		Visited[CurrentHouse] = 1
	return

def main():
	#Create object to track location in 2d space
	CurrentLocation = Location()

	#Initialize starting location to visited
	Visited[CurrentLocation.GetString()] = 1

	#Open file containing moves
	InputFile = open('Day3Input.txt', 'r')

	#Read all moves from file
	Moves = InputFile.read()

	#Process each move
	for Move in Moves:
		if Move == '^':
			CurrentLocation.MoveNorth()
		elif Move == '<':
			CurrentLocation.MoveWest()
		elif Move == '>':
			CurrentLocation.MoveEast()
		else:
			CurrentLocation.MoveSouth()
		SaveVisit(CurrentLocation)

	Counts = Counter(Visited)
	UniqueLocations = list(Counts)
	print len(UniqueLocations)


if __name__ == '__main__':
	main()
