import re
import numpy as np
import matplotlib.pyplot as plt
from time import sleep

def main():

	InputFile = open('Day6Input.txt','r')

	#Create 1000 by 1000 grid of lights that are all initially off
	grid = np.zeros((1000,1000),dtype=bool)

	#Initialize a figure for vizualization
	fig = plt.figure()

	#Create pattern to parse input lines
	pattern = re.compile('(turn on|turn off|toggle).*?(\d+),(\d+).*?(\d+),(\d+)')

	#Process each command in the file
	for line in InputFile:
		ParsedLine = pattern.search(line)
		if ParsedLine:
			Command = ParsedLine.group(1)
			StartingX = np.uint32(ParsedLine.group(2))
			StartingY = np.uint32(ParsedLine.group(3))
			EndingX = np.uint32(ParsedLine.group(4))
			EndingY = np.uint32(ParsedLine.group(5))
			if Command == 'turn on':
				for x in range(StartingX, EndingX + 1):
					for y in range(StartingY, EndingY + 1):
						grid[x][y] = True
			elif Command == 'turn off':
				for x in range(StartingX, EndingX + 1):
					for y in range(StartingY, EndingY + 1):
						grid[x][y] = False
			else:
				for x in range(StartingX, EndingX + 1):
					for y in range(StartingY, EndingY + 1):
						grid[x][y] = ~grid[x][y]
		else:
			print 'Error parsing input line.'


	im = plt.imshow(grid,cmap=plt.cm.Blues)
	plt.show()
	#Close input file
	InputFile.close()

	#Count number of lights that are on
	print str(np.count_nonzero(grid)) + ' lights on.'
	return

if __name__ == '__main__':
	main()
