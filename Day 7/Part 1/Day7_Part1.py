import re
from time import sleep
def Process(wires, signal):
	#This is the base case for recursion
	if isinstance(signal,int) or signal.isdigit():
		return int(signal)
	else:
		print type(signal)
		m = re.search('(?:(?P<lhs>[a-z0-9]*)\s+)?(?P<op>NOT|OR|AND|RSHIFT|LSHIFT)\s+(?P<rhs>[a-z0-9]*)',str(wires[signal]))
		if m:
			d = m.groupdict()
			print d
			if d['op'] == 'NOT':
				val = Process(wires,d['rhs'])
				val = int(val)
				val = ~val
				wires[signal] = val
				return val
			if d['op'] == 'RSHIFT':
				l = Process(wires,d['lhs'])
				r = Process(wires,d['rhs'])
				val = int(l) >> int(r)
				wires[signal] = val
				return val
			if d['op'] == 'LSHIFT':
				l = Process(wires,d['lhs'])
				r = Process(wires,d['rhs'])
				val = int(l) << int(r)
				wires[signal] = val
				return val
			if d['op'] == 'AND':
				l = Process(wires,d['lhs'])
				r = Process(wires,d['rhs'])
				val = int(l) & int(r)
				wires[signal] = val
				return val
			if d['op'] == 'OR':
				l = Process(wires,d['lhs'])
				r = Process(wires,d['rhs'])
				val =  int(l) | int(r)
				wires[signal] = val
				return val
		else:
			return Process(wires,wires[signal])


def main():
	#Open input file for reading
	InputFile = open('input.txt','r')

	#Create a dictionary of wire names
	wires = {}

	#Parse all operations in file
	for Line in InputFile:
		op, name = Line.strip().split('->')
		name = name.strip()
		wires[name] = op.strip()

	for k,v in wires.iteritems():
		if not str(v).isdigit():
			Process(wires,k)

	for k,v in wires.iteritems():
		print k, v
	print wires['lw']
	print wires['lv']
	#Print out result
	print wires['a']

	#Close file when done
	InputFile.close()
	return


if __name__ == '__main__':
	main()
