import re

def main():
	#Open file for reading
	InputFile = open('input.txt','r')

	#Get the string between enclosing quotes
	pattern = re.compile('\"(.*)\"')

	#Initialize counts
	CodeChars = 0
	MemChars = 0

	#Parse each line in input file
	for Line in InputFile:
		#Search current line for quote enclosed string
		s = pattern.search(Line.strip())
		if s:
			#Get number of chars between quotes plus 2 for quotes themselves
			CodeChars = CodeChars + len(s.group(1)) + 2
			#Unescape the string
			decoded = s.group(1).decode('string_escape')
			MemChars = MemChars + len(decoded)
		else:
			print 'No match found.'

	print CodeChars - MemChars
	InputFile.close()

if __name__ == '__main__':
	main()
