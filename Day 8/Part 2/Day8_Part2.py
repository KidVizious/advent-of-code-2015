import re

def main():
	#Open file for reading
	InputFile = open('input.txt','r')

	#Get the string between enclosing quotes
	pattern = re.compile('(\".*\")')

	#Initialize counts
	CodeChars = 0
	EncodedChars = 0

	#Parse each line in input file
	for Line in InputFile:
		#Search current line for quote enclosed string
		s = pattern.search(Line.strip())
		if s:
			#Get number of chars between quotes plus 2 for quotes themselves
			CodeChars = CodeChars + len(s.group(1))
			#Escape the string
			encoded = s.group(1).encode('string_escape')
			encoded = encoded.replace('\"','\\"')
			encoded = '\"' + encoded + '\"'
			EncodedChars = EncodedChars + len(encoded)
			print s.group(1), encoded
			print len (s.group(1)), len(encoded)

		else:
			print 'No match found.'

	print EncodedChars - CodeChars
	InputFile.close()

if __name__ == '__main__':
	main()
