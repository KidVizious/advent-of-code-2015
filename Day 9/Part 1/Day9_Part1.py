import re
from time import sleep

def main():
	routes = []
	cities = []
	InputFile = open('input.txt','r')

	pattern = re.compile('(?P<start>.*)\s+to\s+(?P<end>.*)\s+=\s+(?P<dist>\d+)')
	for Line in InputFile:
		matches = re.search(pattern,Line)
		if matches:
			d = matches.groupdict()
			routes.append((d['start'],d['end'],int(d['dist'])))
			routes.append((d['end'],d['start'],int(d['dist'])))
			if d['start'] not in cities:
				cities.append(d['start'])
			if d['end'] not in cities:
				cities.append(d['end'])
		else:
			print 'No match found.'

	#Sort list of routes by distance
	routes = sorted(routes, key=lambda x: x[2])

	#List to hold distances of each route
	distances = []

	#Try starting at each city in turn
	for starting_city in cities:
		visited = []
		city = starting_city
		distance = 0
		


	InputFile.close()
if __name__ == '__main__':
	main()
